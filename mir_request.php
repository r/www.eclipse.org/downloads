<?php
/**
 * Copyright (c) 2013-2015, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Christopher Guindon (Eclipse Foundation)- initial API and implementation
 * Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
$App = new App();
$Nav = new Nav();
$Theme = $App->getThemeClass();
include ("_projectCommon.php");


$pageTitle = "Eclipse mirror sites";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords('eclipse,download,mirror');
$Theme->setPageAuthor('Eclipse Foundation, Inc.');

require_once "/home/data/httpd/eclipse-php8-classes/system/dbconnection_rw.class.php";
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/classes/mirrors/mirror.class.php");

$_status = $App->getHTTPParameter('status');

$error = 0;

if ($_status == "") {
  $_status = "new";
}

$dbc = new DBConnectionRW();
$dbh = $dbc->connect();

$App = new App();
$Mirror = new Mirror();

if ($_status == "save") {

  $MIR_REQ = $_COOKIE['MIR_REQ'];
  if ($MIR_REQ == "") {

    $valid = true;
    $errorfield = "";
    $alnumspace = "^[][a-zA-Z0-9 .'&,_-]+$";

    // validate incoming
    if (!eregi($alnumspace, $_POST['organization'])) {
      $errorfield .= " Organization";
      $valid = false;
    }
    if (!eregi('^[a-zA-Z]{2}$', $_POST['ccode'])) {
      $errorfield .= " Country";
      $valid = false;
    }
    if (!eregi($alnumspace, $_POST['contact'])) {
      $errorfield .= " Contact";
      $valid = false;
    }

    if (!eregi("^https:\/\/", $_POST['http_browserlink']) && $_POST['http_browserlink'] != "") {
      $errorfield .= " HTTP URL";
      $valid = false;
    }
    if (!eregi('^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z.]{2,5}$', $_POST['email'])) {
      $errorfield .= " E-mail address";
      $valid = false;
    }

    if (!$Mirror->isValidPublicIP($_POST['update_ip_allow'])) {
      $errorfield .= " IP Address";
      $valid = false;
    }

    if ($valid) {
      $sql = "
              INSERT INTO mirrors (
                organization,
                ccode,
                update_ip_allow,
                email,
                date_enabled,
                create_status,
                contact)
              VALUES (
                " . $App->returnQuotedString($_POST['organization']) . ",
                " . $App->returnQuotedString($_POST['ccode']) . ",
                " . $App->returnQuotedString($_POST['update_ip_allow']) . ",
                " . $App->returnQuotedString($_POST['email']) . ",
                NOW(),
                " . $App->returnQuotedString("wait") . ",
                " . $App->returnQuotedString($_POST['contact']) . ")
        ";
      mysqli_query($dbh, $sql);
      $mirror_id = mysqli_insert_id($dbh);

      // add protocols.
      if ($_POST["http_browserlink"] != "") {
        $sql = "
                  INSERT INTO mirror_protocols (
                    mirror_id,
                    protocol,
                    base_path)
                  VALUES (
                    " . $mirror_id . ",
                    " . $App->returnQuotedString("https") . ",
                    " . $App->returnQuotedString($_POST['http_browserlink']) . ")
          ";
        mysqli_query($dbh, $sql);

        $drop_list = $_POST["drop_id"];
        foreach (split(",", $drop_list) as $drop) {
          $sql = "
                    INSERT INTO mirror_drops (
                      mirror_id,
                      drop_id,
                      protocol)
                    VALUES (
                      " . $mirror_id . ",
                      " . $App->returnQuotedString("EclipseFull") . ",
                      " . $App->returnQuotedString("https") . ")";
          mysqli_query($dbh, $sql);
        }
      }
      // Place cookie on browser to avoid having multiple submissions
      setcookie("MIR_REQ", "1");
    }
    else {
      $error = 2;
    }
  }
  else {
    $error = 1;
  }
}
else {
  $sql = "SELECT
          DRP.drop_id,
          DRP.project_id,
          DRP.description,
          IFNULL((
            SELECT
              disk_space
            FROM project_disk_space_usage AS DSK
            WHERE DSK.project_id = DRP.project_id AND DSK.is_quota=1
            )
            ,DRP.size) AS size,
          DRP.size_unit,
          DRP.is_default_selection,
          PRJ.level,
          PRJ.sort_order
        FROM drops AS DRP
          LEFT JOIN projects as PRJ ON PRJ.project_id = DRP.project_ID
        ORDER BY DRP.sort_order
    ";

  $rs = mysqli_query($dbh, $sql);

  $sql = "SELECT
          ccode,
          en_description
        FROM SYS_countries
        ORDER BY en_description
    ";

  $rs_country = mysqli_query($dbh, $sql);
}

// Place your html content in a file called content/en_pagename.php
include ("content/en_" . $App->getScriptName());

$dbc->disconnect();
$dbc = null;

$Theme->setHtml($html);
$Theme->setNav($Nav);
$Theme->generatePage();
