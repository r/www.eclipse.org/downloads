<?php                                                              require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");  require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");   $App   = new App();  $Nav  = new Nav();  $Menu   = new Menu();    include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010-2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Denis Roy (Eclipse Foundation) - initial API and implementation
 *******************************************************************************/

  header("Cache-control: no-cache");

  require_once "/home/data/httpd/eclipse-php8-classes/system/dbconnection.class.php";
  require_once "/home/data/httpd/eclipse-php8-classes/system/dbconnection_rw.class.php";

  $_type   = $App->getHTTPParameter("type");
  $_debug  = $App->getHTTPParameter("debug");
  $_file   = $App->getHTTPParameter("file");

  $cache_hit_status = "CACHE_HIT";

  if($_debug == 1) {
    error_reporting(E_ALL);
     ini_set("display_errors", true);
  }

  # make sure some incoming params are sane
  if($_type != "md5" && $_type != "sha1" && $_type != "sha512") {
    $_type   = "sha512";
  }
  # strip potentially bad characters from file
  $_file = str_replace("\%", "", $_file);
  $_file = str_replace("../", "", $_file);
  $_file = str_replace("'", "", $_file);

  $update_ts = true;

  $filename_fileonly   = substr($_file, strrpos($_file, "/") + 1);  # filename portion only


  # Connect to databases
  # dbc is a read-only database (good for slave servers)
  $dbc    = new DBConnection();
  $dbh     = $dbc->connect();
  $app = new App();

  header("Content-type: text/plain");

  if($_file != "") {
    # locate file on download.eclipse.org to ensure it's valid
    $file_absolute_path = $App->getDownloadBasePath() . $_file;
    if(!file_exists($file_absolute_path)) {
      # check the archives
      $file_absolute_path = "/home/data2/httpd/archive.eclipse.org" . $_file;
      if(file_exists($file_absolute_path)) {
        # don't update the timestamp if the file is in the archives.
        # that will cause problems for download.php
        $update_ts = false;
      }
      else {
        echo "Unable to retrieve or compute sums for " . $_file;
        exit;
      }
    }

    # now we know the file exists
    $file_id   = 0;
    $filetime   = 0;
    $filetime_disk = @filemtime($file_absolute_path);
    $md5sum    = 0;
    $sha1sum  = 0;
    $sha512sum  = 0;

    # q1: get file info from slave
    $sql = "SELECT IDX.file_id, IDX.timestamp_disk, IF(IDX.md5sum = '0', '', IDX.md5sum) AS md5sum, IF(IDX.sha1sum = '0', '', IDX.sha1sum) AS sha1sum, IF(IDX.sha512sum = '0', '', IDX.sha512sum) AS sha512sum FROM download_file_index AS IDX WHERE IDX.file_name = '$_file'";
    if($_debug == 1) {
      echo $sql . "<br />";
    }
    # Get the file_id from the slave, but if the file_id is 0, then re-check the master just in case
    $rs = mysqli_query($dbh, $sql);

    if($myrow = mysqli_fetch_assoc($rs)) {
      $file_id   = $myrow['file_id'];
      $filetime   = $myrow['timestamp_disk'];
      $md5sum    = $myrow['md5sum'];
      $sha1sum  = $myrow['sha1sum'];
      $sha512sum  = $myrow['sha512sum'];

      # Bug 460490: Recalculate if our cached timestamp is older than timestamp of file on disk
      if( ($filetime_disk > $filetime || $md5sum == "" || $sha1sum == "" || $sha512sum == "" || $md5sum == "nofile" || $sha1sum == "nofile" || $sha512sum == "nofile") && $file_id > 0) {

      # compute immediately
        $cache_hit_status = "CACHE_MISS";

        $dbc_RW    = new DBConnectionRW();
        $dbh_RW   = $dbc_RW->connect();

        # lock table so no one else thinks the sums are out of whack for this file
        $sql = "LOCK TABLES download_file_index WRITE";
        mysqli_query($dbh_RW, $sql);

        $md5sum   = md5_file($file_absolute_path);
        $sha1sum   = sha1_file($file_absolute_path);
        $sha512sum   = hash_file('sha512', $file_absolute_path);

        if($md5sum && $sha1sum && $sha512sum) {
          $str = "";
          if($update_ts) {
            $str = "timestamp_disk = '$filetime_disk', ";
          }
          $sql = "UPDATE download_file_index SET $str md5sum = '$md5sum', sha1sum = '$sha1sum', sha512sum = '$sha512sum' WHERE file_id = " . $file_id;
          mysqli_query($dbh_RW, $sql);

          if($_debug == 1) {
            echo $sql . "<br />";
          }
        }
        else {
          echo "Unable to calculate sums for this file (3122).";
        }
        mysqli_query($dbh_RW, "UNLOCK TABLES");
      }


      header("X-Cache-Status: " . $cache_hit_status);

      if($_type == "md5") {
        echo $md5sum . "  " . $filename_fileonly;
      }
      if($_type == "sha1") {
        echo $sha1sum . "  " . $filename_fileonly;
      }
      if($_type == "sha512") {
        echo $sha512sum . "  " . $filename_fileonly;
      }
    }
    elseif ( $update_ts === false ) {
      #file is on archive so generate from there, but only the sum type being requested.
      $hash = "(3119) Couldn't compute hash value for";
      if($_type == "md5") {
        $hash = md5_file($file_absolute_path);
      }
      if($_type == "sha1") {
        $hash = sha1_file($file_absolute_path);
      }
      if($_type == "sha512") {
        $hash = hash_file('sha512', $file_absolute_path);
      }
      echo $hash . "  " . $filename_fileonly;
    }
    else {
      echo "Unable to locate sums for this file (3120).";
    }
  }

  $dbc->disconnect();  # disconnects all pending DB connections
  $rs = null;
  $dbh_RW = null;
  $dbc_RW = null;
  $dbh = null;
  $dbc = null;
?>