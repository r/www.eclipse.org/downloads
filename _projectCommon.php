<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation) - Initial implementation
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");

$App->Promotion = TRUE;
$theme = NULL;
$Nav = new Nav();
$Nav->addNavSeparator("Downloads Home", "/downloads/");
$Nav->addCustomNav("Source code", "http://wiki.eclipse.org/Git", "_self", 1);
$Nav->addCustomNav("More Packages", "/downloads/packages/", "_self", 1);