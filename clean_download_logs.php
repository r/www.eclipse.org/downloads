<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'

    #*****************************************************************************
    #
    # download.php
    #
    # Author:       Denis Roy
    # Date:         2004-11-23
    #
    # Description:  Clean download logs.
    #
    #*****************************************************************************

	# Note: run this from wget -- turn off MYSQLMON.PL on dbslave first!
    exit;
	error_reporting(E_ALL); ini_set("display_errors", true);
	header("Content-type: text/plain");
	require_once "/home/data/httpd/eclipse-php8-classes/system/dbconnection_rw.class.php";
	require_once "/home/data/httpd/eclipse-php8-classes/system/dbconnection.class.php";

	$app = new App();


	$dbc_RW 	= new DBConnectionRW();
	$dbh_RW		= $dbc_RW->connect();
	$dbc_RO 	= new DBConnection();
	$dbh_RO		= $dbc_RO->connect();

	# Get the current downloads table
	$SQL = "SELECT key_value FROM SYS_variables WHERE key_name = 'download_table'";
	$rs = mysqli_query($dbh_RW, $SQL);
	$myrow = mysqli_fetch_assoc($rs);
	$download_table = "downloads" . $myrow['key_value'];
	echo "Using " . $download_table . "\n";

	# HAS TO BE ASCENDING@
	$SQL = "SELECT * FROM download_file_index  ORDER BY file_id  ";
	echo $SQL . "\n";
	$rs 	= mysqli_query($dbh_RW, $SQL);

	while($myrow = mysqli_fetch_assoc($rs)) {
		echo $myrow['file_id'] . " " . $myrow['file_name'];

		# Find duplicate files
		$SQL = "SELECT file_id from download_file_index where file_id > " . $myrow['file_id'] . " AND file_name = " . $app->returnQuotedString($myrow['file_name']);
		$rs2 = mysqli_query($dbh_RW, $SQL) or die(mysqli_error($dbh_RW));
		$str_dupes = "";
		while($myrow2 = mysqli_fetch_assoc($rs2)) {
			if($str_dupes != "") {
				$str_dupes .= ",";
			}
			$str_dupes .= $myrow2['file_id'];
		}

		if($str_dupes != "") {
			echo "--> Dupes: " . $str_dupes . "\n\n";
			# move downloads for the dupes
			$SQL = "UPDATE downloads SET file_id = " . $myrow['file_id'] . " WHERE file_id IN ($str_dupes)";
			echo $SQL . "\n";
			mysqli_query($dbh_RW, $SQL);
			$SQL = "UPDATE " . $download_table . " SET file_id = " . $myrow['file_id'] . " WHERE file_id IN ($str_dupes)";
			echo $SQL . "\n";
			mysqli_query($dbh_RW, $SQL);
			$SQL = "DELETE FROM download_file_index where file_id IN ($str_dupes)";
			echo $SQL . "\n";
			mysqli_query($dbh_RW, $SQL);
		}


		$download_count = 0;
		$SQL = "SELECT COUNT(*) AS RecordCount FROM downloads WHERE file_id = " . $myrow['file_id'];
		$rs2 = mysqli_query($dbh_RO, $SQL) or die(mysqli_error($dbh_RO));
		$myrow2 = mysqli_fetch_assoc($rs2);
		$download_count += $myrow2['RecordCount'];
		echo " Download count main table: " . $download_count;

		$SQL = "SELECT COUNT(*) AS RecordCount FROM " . $download_table . " WHERE file_id = " . $myrow['file_id'];
		$rs2 = mysqli_query($dbh_RW, $SQL) or die(mysqli_error($dbh_RW));
		$myrow2 = mysqli_fetch_assoc($rs2);
		$download_count += $myrow2['RecordCount'];
		echo "  Download count table $download_table : " . $myrow2['RecordCount'];

		echo " TOTAL: " . $download_count . "\n";
		$SQL = "UPDATE download_file_index SET download_count = " . $download_count . " WHERE file_id = " . $myrow['file_id'];
		mysqli_query($dbh_RW, $SQL);



		if($download_count == 0) {
			$SQL = "DELETE FROM download_file_index WHERE file_id = " . $myrow['file_id'];
			echo "----->" . $SQL . "\n";
			$rs3 = mysqli_query($dbh_RW, $SQL);
		}
	}

	$dbc_RW->disconnect();  # disconnects all pending DB connections
	$rs 	= null;
	$dbh_RW	= null;
	$dbc_RW = null;
?>