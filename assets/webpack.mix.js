/*!
 * Copyright (c) 2018 Eclipse Foundation, Inc.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * Contributors:
 *   Christopher Guindon <chris.guindon@eclipse-foundation.org>
 * 
 * SPDX-License-Identifier: EPL-2.0
*/

require('./node_modules/eclipsefdn-solstice-assets/webpack-solstice-assets.mix.js');
let mix = require('laravel-mix');
mix.EclipseFdnSolsticeAssets();

mix.setPublicPath('public');
mix.setResourceRoot('../');

// Default CSS
mix.less("./less/downloads.less", "stylesheets/downloads.min.css");
mix.less("./less/thankyou.less", "stylesheets/thankyou.min.css");
mix.less("./less/mirror.less", "stylesheets/mirror.min.css");

// JavaScript
mix.js('js/eclipsefdn.mirror.js', 'public/javascript/mirror.min.js');

//JavaScript
mix.js('js/eclipsefdn.downloads.js', 'public/javascript/downloads.min.js');

