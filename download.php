<?php
/**
 * Copyright (c) 2006-2017, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation) - initial API and implementation
 *   Christopher Guindon (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

  require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
  require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
  require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
  require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/classes/friends/payment.class.php");

  $App = new App();
  $Nav = new Nav();
  $Menu = new Menu();
  $Paypal = new Paypal();

  include("_projectCommon.php");

  $App->Promotion = FALSE;

  #*****************************************************************************
  #
  # download.php
  #
  # Author:       Denis Roy
  # Date:         2004-11-23
  #
  # Description:  Logic for finding mirror sites that have a specific file
  #
  # HISTORY:    2005-03-19: moved to new infra
  # HISTORY:    2005-04-07: stopped probing external sites
  # HISTORY:    2005-08-13: rewrote selects to use UNIONs instead of temp tables
  # HISTORY:    2005-08-23: added format=plain functionality
  # HISTORY:    2006-01-16: added countryCode functionality
  # HISTORY:    2006-01-25: added GeoIP functionality
  # HISTORY:    2006-01-25: added file_id functionality (1.13)
  # HISTORY:    2006-03-17: added no_mirror and $exclude_string functionality to match RSYNC stanza (1.21)
  # HISTORY:    2006-10-03: added functionality for looking at archive.eclipse.org
  # HISTORY:    2006-12-19: removed file index lookups on slave, as slave lags are bad
  # HISTORY:    2007-02-12: added code to self-clean the download logs
  # HISTORY:    2007-02-12: added holding tables for download logs
  # HISTORY:    2008-03-25: added functionality to return a 404
  # HISTORY:    2009-04-25: added bittorrent functionality
  # HISTORY:    2009-05-26: better sorting of mirrors after my country, my continent
  # HISTORY:    2010-01-13: implement last_known_sync/ztime (bug 291039)
  # HISTORY:    2010-01-13: implement caching of file timestamps (bug 299504)
  # HISTORY:    2010-06-14: Checksums (316820)
  # HISTORY:    2010-09-10: No mirrors for local subnets (324983)
  # HISTORY:    2012-04-05: Throttle IP addresses which frequently abuse site (no referer + multiple accesses per minute = cooldown)
  # HISTORY:    2012-04-17: Provide privileged download access for comitters with their username and password (376732)
  # HISTORY:    2015-06-14: Remove partial mirrors, add support for trends
  #
  #*****************************************************************************

  # Begin: page-specific settings.  Change these.
  $pageTitle = "Eclipse downloads - Select a mirror";
  $pageKeywords = "";
  $pageAuthor = "Denis Roy";

  require_once "/home/data/httpd/eclipse-php8-classes/system/dbconnection_rw.class.php";
  require_once "/home/data/httpd/eclipse-php8-classes/system/dbconnection.class.php";
  require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/classes/mirrors/mirror.class.php");

  $Mirror = new Mirror();

  $archiveBasePath   = "/home/data2/httpd/archive.eclipse.org";
  $archiveBaseURL   = "https://archive.eclipse.org";

  $_format   = $App->getHTTPParameter("format");
  $_debug   = $App->getHTTPParameter("debug");
  $_file     = $App->getHTTPParameter("file");
  $_file_id   = $App->getHTTPParameter("file_id");  # Committers-only fast download

  if ($_format !== 'xml') {
    header("Cache-control: no-cache");
    # Initiate session
    $App->preventCaching();
    $Session = $App->useSession("optional");
    $Friend = $Session->getFriend();
  }
  else {
    // Set cache lifetime to 5 minutes (300 seconds)
    $cache_lifetime = 300;
    // Send cache headers
    header("Cache-Control: max-age={$cache_lifetime}, public, must-revalidate");
    header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $cache_lifetime) . ' GMT');
  }

  //Bug 475734 - download.php should not offer to serve *.php content
  $info = new SplFileInfo($_file);
  if (strtolower($info->getExtension()) == 'php') {
    $_file = '';
  }

  $p2_where = "";

  # don't display mirrors, just redirect
  $_redirect   = $App->getHTTPParameter("r");

  # if the file is invalid, return a 404 instead of a lovely page
  $_send_404   = $App->getHTTPParameter("nf");

  # Identify that the file cannot (or must not) use mirrors.
  # This is for files that are in the exclude list
  $no_mirror = false;

  # to downloads
  $_mirror_id  = $App->getHTTPParameter("mirror_id");
  if($_mirror_id != "" && !preg_match('/^[0-9]+/', $_mirror_id)) {
    $_mirror_id = 1;
    $no_mirror = true;
  }
  $_url  = "";

  if($_debug == 1) {
    echo "Running on hostname " . gethostname() . "Mirror id: " . $_mirror_id . "<br />";
  }


  # extended functionality
  $_country_code  = $App->getHTTPParameter("countryCode");
  $_time_zone    = $App->getHTTPParameter("timeZone");

  # set country_code to bogus if none specified
  if ($_country_code == "") {
    $_country_code = "xx";
  }

  $remote_addr = $App->getRemoteIPAddress();
  $ip_ccode = $Mirror->selectCountryCodeByIP($remote_addr);
  if($_debug == 1) {
    echo "Country given: " . $_country_code . " IP Geolocation: " . $ip_ccode . "<br />";
  }


  if($ip_ccode != "") {
    # We got a country code from the IP. Use it instead.
    $_country_code = $ip_ccode;
  }
  $_country_code = substr($_country_code, 0, 2);

  # project and file index information for logging and statistics
  $file_id   = 0;
  $project_id = "";
  $torrentname= "";


  # Throttle the percentage of downloads we accept for update manager redirects
  $our_download_percentage = 5;

  define('MAX_DOWNLOADS_PER_MINUTE', 60);   # one per second

  # Log download stats, and to which table.  log_download_table will be suffixed with a number later
  $log_download     = !$App->getDBReadOnly();
  $log_download_table = "downloads";

  # Clean up download records older than this number of days
  $download_log_max_age = 365;

  # Do a reverse lookup on the client IP - for internal mirror matching and logging
  $client_hostname = @gethostbyaddr($remote_addr);

  # Get preferred mirror (if any)
  $MIR_PREF = ""; # $_COOKIE['MIR_PREF'];

  $error = "";
  $mtime = 0;

  # make sure some incoming params are sane
  if($_format != "html" && $_format != "xml" && $_format != "plain") {
    $_format   = "html";
  }
  $_protocol = "http";

  # strip potentially bad characters from file
  $_file = str_replace("\%", "", $_file);
  $_file = str_replace("../", "", $_file);
  $_file = str_replace("'", "", $_file);


  # Bypass the entire SQL statement if the file is in the excluded list
  if($Mirror->isExcluded($_file)) {
    # File is in the exclude list. Can't use mirrors, so why not redirect to the actual file?
    # 2008-09-25: What about Friends?
    $no_mirror = true;

    if($_debug == 1) echo "File is excluded from mirrors.<br />";
    # Don't log this download, since listing the file in the download_index will
    # cause the mirror trends to be off
    $log_download = false;
  }

  # Bypass the entire SQL statement if we're getting a HEAD request
  # Bug 479280
  if($_SERVER['REQUEST_METHOD'] == "HEAD") {
    $no_mirror = true;
    if($_debug == 1) echo "HEAD request -- not querying mirrors.<br />";
    $log_download = false;
  }

  # Don't send a mirror list if the request comes from within our network
  if(substr($remote_addr, 0, 10) == "198.41.30."
    || substr($remote_addr, 0, 7) == "172.30."
    || !$Mirror->isValidPublicIP($remote_addr))  {
      $no_mirror = true;
      if($_debug == 1) {
        echo "Coming from " . $remote_addr . "; client should use home site.<br />";
      }
  }

  # Connect to databases
  # dbc is a read-only database (good for slave servers)
  $dbc    = new DBConnection();
  $dbh     = $dbc->connect();

  # this is for logging the request
  $dbc_RW   = new DBConnectionRW();
  $dbh_RW    = $dbc_RW->connect();

  if($_debug == 1 && mysqli_connect_errno()) {
    // Connection failed
    error_log('Connection failed: ' . mysqli_connect_errno());
  }

  $app = new App();

  # find lowest drop location for this file
  # file could be in Release, which is in Full, which is in EclipseFull,
  # so we want to find all the possible mirrors
  $filetime = 0;
  $filesize = 0;
  $filetime_update = false;
  $filename_fileonly = "";  # filename portion only
  if($_file != "") {
    # locate file on download.eclipse.org to ensure it's valid

    $filename     = $app->getDownloadBasePath() . $_file;
    $filename_fileonly   = substr($_file, strrpos($_file, "/") + 1);  # filename portion only


    # find lowest drop and get basic project/file info
    # I originally tried to lump the GeoIP ccode lookup here to save a connection,
    # but the query ended up taking 11 seconds

    # 2008-07-31: broke down the SQL in two different queries
    # using left joins for the download_file_index took .55 sec, two queries uses < .15 sec

    # q1: get file info from slave
    $sql = "SELECT /* download.php lowest_drop */ file_id, timestamp_disk, IF(md5sum = '0', '', md5sum) AS md5sum, IF(sha1sum = '0', '', sha1sum) AS sha1sum, IF(sha512sum = '0', '', sha512sum) AS sha512sum FROM download_file_index WHERE file_name = '$_file' ORDER BY file_id asc LIMIT 1";
    if($_debug == 1) {
        echo $sql . "<br />";
    }
    # Get the file_id from the slave, but if the file_id is 0, then re-check the master just in case
    $rs = mysqli_query($dbh, $sql);

    if($myrow = mysqli_fetch_assoc($rs)) {
      $file_id   = $myrow['file_id'];
      $filetime   = $myrow['timestamp_disk'];
      $md5sum    = $myrow['md5sum'];
      $sha1sum  = $myrow['sha1sum'];
      $sha512sum  = $myrow['sha512sum'];
      if($_debug == 1) {
        echo "Got file ID from secondary DB: $file_id (time: $filetime)<br />";
      }
    }
    else {
      if($_debug == 1) {
        echo "Failed to get file info on secondary DB. Checking the Primary DB.<br />";
      }
      # Lock tables for write in case it's a new file
      # todo: this is not Galera-safe
      $rs = mysqli_query($dbh_RW, "LOCK TABLES download_file_index WRITE");
      $rs = mysqli_query($dbh_RW, $sql);
      if($myrow = mysqli_fetch_assoc($rs)) {
        $file_id   = $myrow['file_id'];
        $filetime   = $myrow['timestamp_disk'];
        $md5sum    = $myrow['md5sum'];
        $sha1sum  = $myrow['sha1sum'];
        $sha512sum  = $myrow['sha512sum'];
        if($_debug == 1) {
            echo "Got file ID from primary DB: $file_id (time: $filetime)<br />";
        }
      }
      else {
        # this is a new file. Needs to be added to index, which is the next step.
        $file_id = addFileToIndex($_file);
        if($_debug == 1) {
          echo "No file information from the database. Adding to index to clear table locks";
        }
      }
    }

    # No file time in the index.  Check the filesystem
    if($filetime == 0) {
      if($_debug == 1) {
        echo "Checking download filesystem for $filename time.<br />";
      }
      if(@fopen($filename, "r")) {
        $filetime = @filemtime($filename);
        $filesize = @filesize($filename);
      }

      if(!$filetime || $filetime == 0) {
        if($_debug == 1) {
          echo "Checking archive filesystem for $filename time.<br />";
        }
        # check on archive.eclipse.org
        $filename = $archiveBasePath . $_file;
        if(@fopen($filename, "r")) {
          $filetime = @filemtime($filename);
          $filesize = @filesize($filename);

          # File exists on archive.eclipse.org.  Since there are no mirrors, redirect to the actual file.
          if($_format == "xml") {
            # we are being asked for an xml list of mirrors for a file that we've established has none.
            # we must unset $rs (the mysql resource) as it contains a resultset for the lowest_drop query, not the mirrors query
            unset($rs);
            $app->sendXMLHeader();
            include("content/en_mir_list_xml.php");
            return false;
          }
          else {
            header("Location: " . $archiveBaseURL . $_file);
            exit;
          }
        }
        else {
          # File isn't on download nor on archive.
          mysqli_query($dbh_RW, "UNLOCK TABLES");

          $error = 1;
          if($_format == "xml") {
            $app->sendXMLHeader();
            include("content/en_mir_list_xml.php");
          }
          else {
            if($_send_404) {
              header("HTTP/1.0 404 Not Found");
              echo "<html><head><title>404 Not Found</title></head><body><h1>404 Not Found</h1></body></html>";
            }
            else {
              include("content/en_mir_download_invalid_file.php");
            }
          }
          return FALSE;
        }
      }
      # We have a file_id, but the timestamp was zero. Update the index
      $filetime_update = true;
    }


    $sql = "SELECT key_value FROM SYS_variables WHERE key_name = 'download_table' LIMIT 1";
    $rs = mysqli_query($dbh, $sql);
    if($myrow = mysqli_fetch_assoc($rs)) {
      $key_value   = $myrow['key_value'];
    }
    if($_debug == 1) {
      echo "Project SQL: ". $sql . "<br /><br />";
    }
    $log_download_table .= $key_value;

  }
  if($_debug == 1) {
    echo "File ID: " . $file_id . "<br />";
    echo "Log download table: " . $log_download_table . "<br />";
  }

  # First select: get mirrors at the lowest drop
  $having = "((MIR.is_internal <> 1 AND last_known_sync > " . $filetime . ")
            OR LOCATE(MIR.internal_host_pattern, '" . $client_hostname . "') > 1) AND success_rate > 80 ";

  $where = " MRD.drop_id = 'EclipseFull'";
  if($_protocol != "") {
    $where .= " AND MRP.protocol LIKE '" . $app->sqlSanitize($_protocol, $dbh_RW) . "%'";
  }
  if($_mirror_id > 0) {
    $where .= " AND MIR.mirror_id = '$_mirror_id'";
  }
  $eclipseFullDropSQL = "SELECT /* download.php:$remote_addr */
          IF(LOCATE(MIR.internal_host_pattern, '" . $client_hostname . "') > 1, 1, 0) AS SuggestInternal,
          IF(COU.ccode = '$_country_code', 1, 0) AS SameCountry,
          IF(COU.continent_code = MYCOU.continent_code, 1, 0) AS SameContinent,
          ROUND(AVG(TRD.download_speed_kbps),0) * RAND() AS avg_speed_indexed, SUM(IF(TRD.http_status_code BETWEEN 200 AND 299, 1, 0)) / COUNT(TRD.trend_id) * 100 AS success_rate,
          MIR.mirror_id,
          MIR.organization,
          MIR.ccode,
          MIR.is_internal,
          MIR.internal_host_pattern,
          MRD.drop_id,
          DRP.our_path,
          DRP.file_pattern,
          MRP.protocol,
          MRP.base_path,
          MRD.rel_path_override,
          MRD.timestamp,
          IF(MRD.timestamp > MRD.timestamp_end, IF(MRD.timestamp_last_complete > 0, MRD.timestamp_last_complete, IF(MRD.timestamp_end > 0,  MRD.timestamp_end, 0)), MRD.timestamp) AS last_known_sync,
          COU.en_description AS country_desc,
          CON.en_description AS continent_desc,
          CON.sort_order
        FROM mirrors AS MIR
          INNER JOIN mirror_protocols AS MRP on MRP.mirror_id = MIR.mirror_id
          INNER JOIN mirror_drops AS MRD ON MRD.mirror_id = MIR.mirror_id
            AND MRD.protocol = MRP.protocol
          INNER JOIN drops as DRP ON DRP.drop_id = 'EclipseFull'
          INNER JOIN SYS_countries AS COU ON COU.ccode = MIR.ccode
          INNER JOIN SYS_continents AS CON ON CON.continent_code = COU.continent_code
          LEFT JOIN SYS_countries AS MYCOU ON MYCOU.ccode = '$_country_code'
          LEFT JOIN mirror_trends AS TRD ON TRD.mirror_id = MIR.mirror_id
        WHERE
          MIR.is_advertise = 1
          AND MIR.create_status = 'active'
          AND $where
        GROUP BY MIR.mirror_id
        HAVING " . $having;


  $orderby = " is_internal DESC, SuggestInternal DESC, SameContinent DESC, CON.sort_order DESC, success_rate DESC, avg_speed_indexed DESC";

  if($_redirect == 1) {
    $orderby .= " LIMIT 1";
  }

  $mirrorListSQL = $eclipseFullDropSQL . " ORDER BY " . $orderby;

  # Bypass the whole mirror SQL if we can't pick a mirror for this file
  $rs;
  if(!$no_mirror && $_mirror_id != 1) {   #mirror_id 1 is download
    $rs   = mysqli_query($dbh, $mirrorListSQL);
    $myrow = mysqli_fetch_assoc($rs);
    mysqli_data_seek($rs, 0);
    $_url = $myrow['base_path']. $_file;
    if($_redirect == 1) {
      $_mirror_id = $myrow['mirror_id'];
    }
  }
  else {
    $_url = $app->getPubDownloadServerUrl() . $_file;
    # Bug 470444 - download of ZIP archives doesn't work anymore
    $_redirect = 1;
  }

  if($_debug == 1) {
    echo $mirrorListSQL . "<br /><br />";
  }


  if($_redirect == 1) {
    # fetch row & build URL
    # Higher weight = more to our server.  Set weight above ($our_download_percentage)
    $random =  rand(1, 100);

    # download from eclipse.org
    if($random <= $our_download_percentage || $no_mirror || mysqli_num_rows($rs) == 0) {
      $_mirror_id = 1;
      $_url = $app->getPubDownloadServerUrl() . $_file;
    }

    if($_debug == 1) {
      echo "Redirect: $_url <br /><br />";
    }

  }
  if($_debug == 1) {
    echo "Pre phase-II<br /> URL: $_url <br />File: $_file:<br />Mirror: $_mirror_id";
  }

  # Phase II - a user picked a mirror for his file, or redirect, or there are no mirrors
  # ready the download, and log it
  if(($_file != "" && $_mirror_id > 0 && $_url != "" && $_format != "xml") || $_debug) {
    $ip     = $remote_addr;
    $cnt    = 0;   # recent downloads

    # Throttle the amount of downloads we count (and redirect to) to prevent ballot stuffers
    $sql = "SELECT file_id, COUNT(1) AS cnt FROM $log_download_table WHERE remote_addr = '$ip' AND download_date > DATE_SUB(NOW(), INTERVAL 1 MINUTE) GROUP BY file_id, remote_addr HAVING cnt > " . MAX_DOWNLOADS_PER_MINUTE;
    $rs = mysqli_query($dbh, $sql);
    if($myrow = mysqli_fetch_assoc($rs)) {
      include("content/en_too_many_downloads.php");
    }
    else {
      # Successful download. Log, and day goodbye
      # this code is more reliably run before the header is sent
      if($_debug == 1) {
        echo "Logging the download if log_download is true<br>";
      }
      if ($log_download && !empty($file_id)) {
        $sql = "INSERT INTO $log_download_table (file_id, download_date, remote_host, remote_addr, mirror_id, ccode)
        VALUES ( $file_id, NOW(), '$client_hostname', '$ip', $_mirror_id, '$_country_code')  ";
        mysqli_query($dbh_RW, $sql);
        $string = "";
        if($filetime_update) {
          $string = ", timestamp_disk = $filetime";
        }
        $sql = "UPDATE LOW_PRIORITY download_file_index SET download_count = download_count + 1 " . $string . " WHERE file_id = " . $file_id;
        mysqli_query($dbh_RW, $sql);
      }

      if($_redirect == 1) {
        # Blindly send user off to a mirror site
        header("Location: " . $_url);
        $is_success = 2;
      }
      else {
        $is_success = 2;

        # Redirect user to the file instead of an HTML/meta redirect
        # Bug 134634
        # include("content/en_mir_download_success.php");
        if (!isset($_COOKIE['thankyou_page']['eclipse_donation']) || !isset($_COOKIE['thankyou_page']['eclipse_newsletter'])) {
          include("content/en_mir_download_success.php");
        }
        else {
          header("Location: " . $_url);
        }
      }
    }

    if($_debug == 1) {
      echo $sql . mysqli_error($dbh_RW) . "<br />";
    }
  }
  else {
    if($_file != "") {
      if($_format == "html") {

        // Bug 484100 - Removed the Download button from mirror pages
        $variables = array();
        $variables['btn_cfa'] = array(
          'hide' => TRUE, // Optional - Hide the CFA button.
        );
        $App->setThemeVariables($variables);

        // Place your html content in a file called content/en_pagename.php
        ob_start();
        include("content/en_" . $App->getScriptName());
        $html = ob_get_clean();
        # Generate the web page
        $App->AddExtraJSFooter('<script type="text/javascript" src="/downloads/assets/public/javascript/mirror.min.js"></script>');
        $App->AddExtraHtmlHeader('<link href="/downloads/assets/public/stylesheets/mirror.min.css" media="screen" rel="stylesheet" type="text/css"/>');
       // Place your html content in a file called content/en_pagename.php
        $App->generatePage($theme, $Menu, NULL, $pageAuthor, $pageKeywords, $pageTitle, $html);
      }
      if($_format == "plain") {
        include("content/en_mir_list_plain.php");
      }
      if($_format == "xml") {
        $app->sendXMLHeader();
        include("content/en_mir_list_xml.php");
      }
    }
    else {
      if($_format == "html") {
        if($_send_404) {
          header("HTTP/1.0 404 Not Found");
          echo "<html><head><title>404 Not Found</title></head><body><h1>404 Not Found</h1></body></html>";
        }
        else {
          include("content/en_mir_download_invalid_file.php");
        }
      }
      if($_format == "xml") {
        $app->sendXMLHeader();
        include("content/en_mir_list_xml.php");
      }
    }
  }

  /**
   *
   * @param $filename - file name portion (relative to http://download.eclipse.org) to inspect
   * @param $trainname - optional name of train to limit search to
   * @return string - name of package (jee|java|cpp|modeling|reporting|rcp|php|pulsar|sdk) or the empty string
   * @author Denis Roy
   * @example getPackageFromFilename("/technology/epp/downloads/release/galileo/R/eclipse-rcp-galileo-R-win32.zip", "galileo")
   * @since June 2009
   * @
   */
  function getPackageFromFilename($filename, $trainname=".*") {
    $rValue = "";

    $packageList = "automotive|jee|java|cpp|modeling|reporting|rcp|scout|testing|soa|javascript|parallel|php|committers";

    # /technology/epp/downloads/release/galileo/R/eclipse-rcp-galileo-RC4-win32.zip
    # /eclipse/downloads/drops/R-3.4-200806172000/eclipse-SDK-3.4-solaris-gtk.zip
    # technology/epp/downloads/release/kepler/R/eclipse-reporting-kepler-
    if(preg_match(",^/technology/epp/downloads/release/($trainname)/R/eclipse-($packageList)-($trainname),", $filename, $matches)) {
      $rValue = $matches[2];
    }
    elseif(preg_match(",^/oomph/epp/($trainname)/R/eclipse-inst-,", $filename)) {
      $rValue = "installer";
    }
    return $rValue;
  }

  function getOSFromFilename($filename) {
    $rValue = "";

    $osList = "win32-x86_64|win32|macosx-cocoa|linux-gtk-x86_64|linux-gtk.tar|linux64|linux32|win64|win32|mac64|aarch64";

    # /technology/epp/downloads/release/galileo/R/eclipse-rcp-galileo-RC4-win32.zip
    # /eclipse/downloads/drops/R-3.4-200806172000/eclipse-SDK-3.4-solaris-gtk.zip

    if(preg_match(",($osList),", $filename, $matches)) {
      $rValue = $matches[1];
    }

    switch($rValue) {
      case "linux-gtk-x86_64": $rValue = "LINUX64"; break;
      case "linux64": $rValue = "LINUX64"; break;
      case "linux-gtk.tar": $rValue = "LINUX32"; break;
      case "linux32": $rValue = "LINUX32"; break;
      case "macosx-cocoa-x86_64": $rValue = "MACCOCOA"; break;
      case "mac64": $rValue = "MACCOCOA"; break;
      case "macosx-cocoa": $rValue = "MACCOCOA"; break;
      case "win32-x86_64": $rValue = "WIN64"; break;
      case "win64": $rValue = "WIN64"; break;
      case "win32": $rValue = "WIN32"; break;
      case "aarch64": $rValue = "AARCH64"; break;
    }

    return $rValue;
  }

  function logDownload($in_file_id, $in_mirror_id, $ip = NULL) {
    # todo: delete this function, it is never called.
    if($_debug == 1) {
      echo "This is unused codecode<br />";
    }
    if (is_null($ip)) {
      $ip = $_SERVER['REMOTE_ADDR'];
    }

    global $dbh_RW, $log_download_table, $client_hostname, $_country_code, $_debug;


    if ($in_file_id != "") {
      $sql = "INSERT INTO $log_download_table (file_id, download_date, remote_host, remote_addr, mirror_id, ccode)
        VALUES ( $in_file_id, NOW(), '$client_hostname', '$ip', $in_mirror_id, '$_country_code')  ";
      if($_debug == 1) {
        echo $sql . "<br />";
      }
      mysqli_query($dbh_RW, $sql);

      $sql = "UPDATE LOW_PRIORITY download_file_index SET download_count = download_count + 1 WHERE file_id = " . $in_file_id;
      mysqli_query($dbh_RW, $sql);
    }
  }

  function addFileToIndex($in_file) {
    global $app, $dbc_RW, $dbh_RW, $filetime, $filesize, $_debug;

    if ($_debug == 1) {
      error_log('Adding file to index: addFileToIndex()');
      error_log('File ($in_file): ' . var_export($in_file, TRUE));
      error_log('Filetime ($filetime): ' . var_export($filetime, TRUE));
      error_log('Filesize ($filesize): ' . var_export($filesize, TRUE)); 
    }

    # Add this file to the file index
    if($in_file != "") {
      $sql = "INSERT INTO download_file_index (file_id, file_name, download_count, timestamp_disk, size_disk_bytes)
        VALUES (NULL, '" . $app->sqlSanitize($in_file, $dbh_RW) . "', 0, $filetime, $filesize)";
      $rs = mysqli_query($dbh_RW, $sql);
      $inserted_id = mysqli_insert_id($dbh_RW);
      mysqli_query($dbh_RW, "UNLOCK TABLES");
      if ($_debug == 1) {
        error_log('Inserted ID ($inserted_id): ' . var_export($inserted_id, TRUE));
        echo "Added file " . var_export($in_file, TRUE) . "to index<br>";
      }
      return $inserted_id;
    }
    else {
      return 0;
    }
  }


  $dbc_RW->disconnect();  # disconnects all pending DB connections
  $rs   = null;
  $dbh_RW  = null;
  $dbc_RW = null;
  $dbh  = null;
  $dbc   = null;