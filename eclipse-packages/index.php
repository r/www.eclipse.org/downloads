<?php
/**
 * Copyright (c) 2014, 2016, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

if (isset($_GET['show_instructions'])) {
  header('Location: /downloads/packages/installer', FALSE, 302);
  exit();
}

header('Location: /downloads/packages', FALSE, 302);
exit();
