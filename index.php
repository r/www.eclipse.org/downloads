<?php
/**
 * Copyright (c) 2014, 2016, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
//require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/classes/downloads/downloads.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/classes/downloads/eclipseInstaller.php");

$App = new App();
$Theme = $App->getThemeClass();
//$Downloads = new Downloads();
$Installer = new EclipseInstaller();
$Installer->setInstallerLayout("layout_a");

// This hide/show the banner ad from being displayed
//$Downloads->hideBannerAd(FALSE);

// @todo: We should probably move these functions to Theme().
$App->Promotion = TRUE;
$App->PageRSSTitle = "Eclipse Releases";

$App->AddExtraJSFooter('<script type="text/javascript" src="/downloads/assets/public/javascript/downloads.min.js?v=1.0"></script>');

$App->setThemeVariables(
  array(
    'main_container_classes' => 'container-full test',
    'hide_breadcrumbs' => TRUE,
  )
);

$pageTitle = 'Eclipse Downloads';
$Theme->setPageAuthor('Eclipse Foundation');
$Theme->setPageKeywords('eclipse, download, downloads, mirror, project, plug-ins, plugins, java, ide, swt, refactoring, free java ide, tools, platform, open source, development environment, development, ide');
$Theme->setPageTitle($pageTitle);

$App->Promotion = FALSE;

// Custom theme variables
$variables = array();
$variables['main_container_classes'] = 'container-full breadcrumbs-offset downloads-container';

// Optional - Hide the CFA button.
$variables['btn_cfa'] = array(
  'hide' => TRUE
);

$App->setThemeVariables($variables);

ob_start();
?>
    <div class="container">
      <div class="download-extra-header row">
        <div class="col-md-12 download-top-banner match-height-item-by-row">
          <img class="img img-responsive" src="images/download-parallax.png" />
        </div>
        <div class="col-md-12 match-height-item-by-row">
          <h1 class="h2 text-center">Download Eclipse Technology that is right for you</h1>
        </div>
      </div>
      <div class="row">
        <div class="eclipsefdn-featured-story" data-template-id="template-featured-story-jumbotron" data-publish-target="eclipse_org">
          <div class="featured-container"></div>
        </div>
      </div>
    </div>
    <!-- Move styles to EFSA -->
    <style>
      .eclipsefdn-featured-story[data-template-id="template-featured-story-jumbotron"] .btn-primary {
        background-color: #000;
        border-color: #000;
      }

      .eclipsefdn-featured-story[data-template-id="template-featured-story-jumbotron"] .featured-story-jumbotron-content {
        width: 100%;
      }

      .eclipsefdn-featured-story[data-template-id="template-featured-story-jumbotron"] .featured-story-jumbotron-links {
        flex-shrink: 0;
      }
    </style>
<?php
$extra_header_html = ob_get_clean();
$Theme->setExtraHeaderHtml($extra_header_html);

// Place your html content in a file called content/en_pagename.php
ob_start();
include ("content/en_index.php");
$html = ob_get_clean();

$Theme->setHtml($html);
$Theme->setExtraHeaders('<link href="/downloads/assets/public/stylesheets/downloads.min.css?v=1.3" media="screen" rel="stylesheet" type="text/css"/>');
$Theme->preventCaching();
$Theme->generatePage();
