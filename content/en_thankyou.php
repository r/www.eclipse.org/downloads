<?php
/*******************************************************************************
 * Copyright (c) 2006, 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Denis Roy (Eclipse Foundation) - initial API and implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/

$title_1 = 'Thank you';
$title_2 = 'for your download!';

// Bug 517126 - Thank you for downloading Eclipse
$show_default_title = $App->getHTTPParameter('default_title');
if ($show_default_title === "false") {
  $title_2 .= ' technology';
}

$domain = $App->getEclipseDomain();

$download_link = "";
if(isset($_url)){
  $download_link = '<p class="download-link-msg">If the download doesn\'t start in a few seconds, please <a href="' . $_url . '">click here</a> to start the download.</p>';
}

// What page to show (donationA - 0%, donationB - 90%, newsletter - 10%)
$display = 'donation'; // 90%

// Don't bother people with donations/newsletter if they have already done so
// Both of these will never be True because of downloads.php:661
if (isset($_COOKIE['thankyou_page']['eclipse_newsletter'])) $display = 'donation';
if (isset($_COOKIE['thankyou_page']['eclipse_donation'])) $display = 'newsletter';

$layouts = array('layout_a','layout_b');
$picked_layout = $layouts[rand(0,1)];

// Cookies for download stats
setcookie ("thankyou_showing", $display, time() + (3600 * 24 * 360 * 10), '/', $domain['cookie']);

$download_page = TRUE;
// Custom theme variables
$variables = array();
$variables['main_container_classes'] = 'container-full';
$App->setThemeVariables($variables);
$App->AddExtraHtmlHeader('<link rel="stylesheet" href="/downloads/assets/public/stylesheets/thankyou.min.css"/>');
include_once($_SERVER['DOCUMENT_ROOT'] . "/sponsor/content/en_index.php");