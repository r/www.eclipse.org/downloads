<?php
/*******************************************************************************
 * Copyright (c) 2014, 2016, 2023 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *******************************************************************************/
?>

<!-- @todo: Remove or update and add .downloads-section below -->

<style>
  .eclipsefdn-featured-story .featured-story-jumbotron-content-body {
    font-weight: 500;
  }
</style>

<script>
  // Parallax effect
  const topBannerImageElement = document.querySelector('.download-top-banner img');

  document.addEventListener('scroll', () => {
    const scrollY = window.scrollY;
    topBannerImageElement.style.transform = `translateX(${scrollY * -0.25}px)`;
  }); 
</script>

<section class="container margin-top-20 margin-bottom-60">
  <div class="row">
    <div class="col-md-12 bg-neutral-flat match-height-item">
      <?php print $Installer->output(); ?>
    </div> 

    <div class="featured-download col-md-12 bg-neutral-flat-dark match-height-item">
      <div class="featured-download-logo">
        <img class="img" src="https://www.eclipse.org/downloads/assets/public/images/logo-temurin.png?version=2" />
      </div>
      <p>
        The Eclipse Temurin&trade; project provides high-quality, TCK
        certified OpenJDK runtimes and associated technology for use across the
        Java&trade; ecosystem.
      </p>
      <div class="btn-group">
        <a class="btn btn-neutral" href="https://projects.eclipse.org/projects/adoptium.temurin">Learn More</a>
        <a class="btn btn-primary" href="https://adoptium.net/">Download</a>
      </div>
   </div> 
  </div>
</section>

<section class="other-products-section container">
  <div class="row">

    <div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-0 match-height-item-by-row">
      <div class="card card-bordered text-center margin-bottom-40">
        <div class="card-logo margin-x-auto margin-bottom-20">
          <img src="assets/public/images/logo-che.png?version=2" alt="Eclipse Che" />
        </div>
        <div class="card-content">
          <p>Eclipse Che is a developer workspace server and cloud IDE</p>
        </div>
        <div class="card-action">
          <a class="btn btn-neutral btn-block" href="https://eclipse.org/che">Learn More</a>
          <a class="btn btn-primary btn-block" href="https://www.eclipse.org/che/getting-started/download/">Download</a>
        </div>
      </div>
    </div>

    <div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-0 match-height-item-by-row margin-bottom-40">
      <div class="card card-bordered text-center">
        <div class="card-logo margin-x-auto margin-bottom-20">
          <img src="assets/public/images/logo-theia.png?version=3" alt="Eclipse Theia IDE" />
        </div>
        <div class="card-content">
          <p>
            A modern and open IDE for cloud and desktop 
          </p>
        </div>
        <div class="card-action">
          <a class="btn btn-neutral btn-block" href="https://theia-ide.org">Learn More</a>
          <a class="btn btn-primary btn-block" href="https://theia-ide.org/#theiaide">Download</a>
        </div>
      </div>
    </div>

    <div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-0 match-height-item-by-row margin-bottom-40">
      <div class="card card-bordered text-center">
        <div class="card-logo margin-x-auto margin-bottom-20">
          <img src="assets/public/images/logo-jetty.png" alt="Eclipse Jetty" />
        </div>
        <div class="card-content">
          <p>Eclipse Jetty provides a web server and javax.servlet container</p>
        </div>
        <div class="card-action">
          <a class="btn btn-neutral btn-block" href="https://eclipse.dev/jetty/">Learn More</a>
          <a class="btn btn-primary btn-block" href="https://www.eclipse.org/jetty/download.html">Download</a>
        </div>
      </div>
    </div>

    <div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-0 match-height-item-by-row margin-bottom-40">
      <div class="card card-bordered text-center">
        <div class="card-logo margin-x-auto margin-bottom-20">
          <img src="assets/public/images/logo-glassfish.png" alt="Eclipse GlassFish" />
        </div>
        <div class="card-content">
          <p>Eclipse GlassFish provides a complete application server which serves the Jakarta EE specification.</p>
        </div>
        <div class="card-action">
          <a class="btn btn-neutral btn-block" href="https://projects.eclipse.org/projects/ee4j.glassfish">Learn More</a>
          <a class="btn btn-primary btn-block" href="https://projects.eclipse.org/projects/ee4j.glassfish/downloads">Download</a>
        </div>
      </div>
    </div>

  </div>
</section>

<!-- Ad Section -->
<div class="row text-center margin-bottom-30">
  <div class="container">
    <div 
      class="eclipsefdn-promo-content" 
      data-ad-format="ads_top_leaderboard" 
      data-ad-publish-to="eclipse_org_downloads"
    >
    </div>
  </div>
</div>

<section class="featured-section-row featured-section-row-dark-bg text-center padding-top-60 padding-bottom-60" id="all-projects">
    <div class="container">
        <h2 id="projects">All Projects</h2>
        <p class="text-primary-light">Find an Eclipse open source project.</p>
        <form class="search-bar" action="https://projects.eclipse.org">
          <input name="combine" type="text" placeholder="Search" />
          <button class="btn btn-primary">
            <i class="fa fa-search" aria-hidden="true"></i>
          </button>
        </form>
        <a class="btn btn-primary" href="//projects.eclipse.org">
            List of Projects
        </a>
    </div>
</section>

