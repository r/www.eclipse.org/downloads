<?php
	#*****************************************************************************
	#
	# en_mir_list.php
	#
	# Author: 		Denis Roy
	# Date:			2004-11-20
	#
	# Description: UI to list mirrors in XML format
	#
	# HISTORY:
	# - 2004-11-25: removed the <mirrors> and </mirrors> markers because platform
	#   team did not want well-formed xml
	#
	#****************************************************************************

	#include("inc/en_doc_header.php");
	#include("inc/en_banner.php");


	echo "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
	echo "<mirrors>";
?>
<?php
        while ($myrow = mysqli_fetch_assoc($rs)) {
          $path = $myrow['base_path'] . $_file;
?>
	<mirror url="<?= $path ?>" label="[<?= $myrow['country_desc'] ?>] <?= str_replace("&", "&amp;", $myrow['organization']) ?> (<?= $myrow['protocol'] ?>)" />
<?php
	}

	echo "</mirrors>";
?>
