<?php
/*******************************************************************************
 * Copyright (c) 2006-2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Denis Roy (Eclipse Foundation) - initial API and implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
  // Record Stats
  $App->AddExtraHtmlHeader('<link href="https://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">');
  $App->AddExtraHtmlHeader('<link rel="stylesheet" href="/downloads/assets/public/stylesheets/thankyou.min.css"/>');
?>
<div class="container-thankyou">
  <?php if (isset($title_1) && isset($title_2) && isset($download_link)):?>
    <div class="row thankyou text-center">
      <h1 class="thankyou-title"><span><?php print $title_1; ?></span> <?php print $title_2;?></h1>
      <span class="direct-link"><?php print $download_link; ?></span>
    </div>
  <?php endif;?>
  <div class="newsletter-body">
    <div class="newsletter">
      <div class="container">
        <div class="eclipse-newsletter-box col-sm-12 col-sm-offset-6">
          <div id="signup_text">
            <span class="signup_1">Eclipse Newsletter</span><br />
            <span class="signup_2">A fresh new issue delivered monthly</span>
            <div class="signup_3">
              <!-- Begin MailChimp Signup Form -->
              <form class="validate form-inline" action="https://eclipsecon.us6.list-manage.com/subscribe/post" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank" novalidate>
                <div class="form-group">
                  <!--  <div id="req_msg"><span class="red">*</span> Indicates required</div>-->
                  <input type="hidden" name="u" value="eaf9e1f06f194eadc66788a85">
                  <input type="hidden" name="id" value="46e57eacf1">
                  <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button btn btn-warning">
                </div>
              </form>
              <!--End mc_embed_signup-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>