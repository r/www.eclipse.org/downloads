<?php
/**
 * Copyright (c) 2015, 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation) - Initial implementation
 *   Christopher Guindon (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

$mainDownloadFile = $app->getPubDownloadServerUrl() . $_file;
$target      = "";
$mirror_id_prev = 0;
$ccode_prev   = "";

$mir_count     = 0;
$mir_internal_count  = 0;

$strFileName = $filename_fileonly;
$strlength = strlen($strFileName);
if ($strlength > 57) {
  $strFileName = substr($strFileName, 0, 30) . "..." . substr($strFileName, $strlength-30, 30);
}

$download_link = array(
  'url' => 'download.php?file=' . $_file,
  'text' => '',
  'icon' => '',
  'suffix' => ''
);

$mirror_id_list  = array();
$mirror_is_available = TRUE;

$commiter_link = '';
if (!$Session->getIsLoggedIn()) {
  $commiter_link = " Eclipse Committers can <a href='" . $Session->getLoginPageURL() . "'>log in</a> for faster download.";
}

# Bug 544049 - Support GeoLite2
# For now, we'll show the first mirror as "preferred" even if we don't know the country.
# if ($_country_code != "xx" && $myrow = mysqli_fetch_assoc($rs)) {
if ($myrow = mysqli_fetch_assoc($rs)) {
  // Preferred mirror
  $download_link['url'] .= "&mirror_id=" . $myrow['mirror_id'];
  $download_link['text'] = '' . $myrow['country_desc'] .' - ' . $myrow['organization'] .' (' . $myrow['protocol'] . ')';
}
else {
  // No mirrors :(
  $mirror_is_available = FALSE;
  if ($Friend->getIsCommitter()) {
    $download_link['url'] .= '&file_id=' . $file_id;
    $download_link['text'] = 'Canada - Committers & Friends fast download from Eclipse.org';
  }
  else {
    $download_link['url'] .= '&mirror_id=1';
    $download_link['text'] = 'Canada - Main Eclipse.org downloads area';
  }
}

$mirrors_internal = array();
$mirrors = array();
while ($myrow = mysqli_fetch_assoc($rs)) {
  $mirror_link = array();
  $internal_mirror_flag = FALSE;
  if (!is_numeric(array_search($myrow['mirror_id'], $mirror_id_list))) {
    $mirror_id_list[count($mirror_id_list)] = $myrow['mirror_id'];
    $path = "download.php?file=" . $_file . "&mirror_id=" . $myrow['mirror_id'];

    if ($myrow['is_internal'] == 1 && $_file != "") {
      # Bypass stats for internal mirrors
      $path = $myrow['base_path'] . getPath($myrow['drop_id'], $_drop_id, $myrow['rel_path_override'], $myrow['our_path'], $myrow['our_path2']);
      $internal_mirror_flag = TRUE;
    }

    $mirror_link = array(
      'path' => $path,
      'country' =>$myrow['country_desc'],
      'target' => $target,
      'protocol' => $myrow['protocol'],
      'organization' => $myrow['organization']
    );

    if ($internal_mirror_flag) {
      $mirrors_internal[$myrow['continent_desc']][$myrow['mirror_id']][]  = $mirror_link;
    }
    else{
      $mirrors[$myrow['continent_desc']][$myrow['mirror_id']][] = $mirror_link;
    }
  }
}

function _print_mirror_list($mirrors = array()) {
  $html = '';
foreach ($mirrors as $mid_mirrors) {
    $html .= '<li class="margin-bottom-10">';
    foreach($mid_mirrors as $key => $m) {
      $link = '<a href="' . $m['path'] . '" target="' . $m['target']. '">';
      if ($key == 0) {
        $html .= $m['country'] . ' - ';
        //$html .= $link . $m['organization'] .' ('. $m['protocol'] .')</a>';
        $html .= $link . $m['organization'] .'</a>';
      }
      else{
        $html .= ' &#160; (' . $link . $m['protocol'] . '</a>)';
      }
    }
    $html .= '</li>'. PHP_EOL;
  }
  return $html;
}

$mirror_html = '';
if (!empty($mirrors)) {
ob_start();
?>
  <h2>Choose a mirror close to you</h2>
  <hr>
  <div class="choose-mirror">
    <?php $x = 0; ?>
    <?php foreach ($mirrors as $continent => $mir):?>
      <?php $x++; ?>
      <div class="mirror-continent mirror-section-<?php print $x;?>">
        <h3><?php print $continent?></h3>
        <ul class="list-unstyled">
          <?php print _print_mirror_list($mir);?>
       </ul>
      </div>
    <?php endforeach;?>
    <a class="btn btn-sm btn-primary" id="show_all_mirrors" href="#show_all_mirrors">Show all</a>
  </div>
  <div class="choose-mirror">
    <h3>Main Download Site</h3>
    <ul class="list-unstyled">
      <li>Canada - <a href="download.php?file=<?print $_file; ?>&mirror_id=1">Main eclipse.org downloads area</a></li>
    </ul>
  </div>
  <?php
  $mirror_html = ob_get_clean();
}
?>
<div class="col-md-18">
  <p>All downloads are provided under the terms and conditions of the <a href="/legal/epl/notice.php">Eclipse Foundation
  Software User Agreement</a> unless otherwise specified.</p>
  <div class="row">
    <div class="col-md-24">
      <div class="mirror-well text-center">
        <a href="<?php print $download_link['url'];?>" class="btn btn-warning margin-bottom-15 btn-lg"><i class="fa fa-download"></i> Download</a>
        <?php if (!$mirror_is_available) :?>
          <p>There are no mirrors for this file.<?php print $commiter_link;?></p>
        <?php endif;?>

        <p><strong>Download from:</strong> <?php print $download_link['icon'];?> <?php print $download_link['text'];?><?php print $download_link['suffix'];?></p>
        <p><strong>File:</strong> <a href="<?php print $download_link['url'];?>" class="orange"><?php print $strFileName;?></a> <?php $sha512sum="nsuidbus"; if ($sha512sum != "") :?><a  class="btn btn-ajax-checksum btn-xs btn-default" data-checksum-file="<?php print $_file;?>" data-checksum-type="sha512" data-toggle="tab" role="tab" aria-controls="settings" href="#btn-ajax-checksum-sha512">SHA-512</a><?php endif;?></p>
        <?php if ($sha512sum != "") :?>
          <div class="tab-content">
          <div id="btn-ajax-checksum-sha512" class="tab-pane fade" role="tabpanel">
            <div class="well well-sha512">
             <p></p>
            </div>
          </div>
          </div>
        <?php endif;?>
        <p><a role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">>> Select Another Mirror</a></p>
        <div class="collapse" id="collapseExample">
          <div class="well choose-mirror-well">
            <i class="fa fa-times-circle close-choose-mirror-well" aria-hidden="true"></i>
            <?php print $mirror_html; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="download-faster col-md-24">
      <?php if (!empty($mirrors_internal)) :?>
        <div class="choose-mirror">
          <h2 class="mirror-members-h2">The following Internal Mirrors should be used for this file</h2>
          <?php foreach ($mirrors_internal as $continent => $mir):?>
            <h3><?php print $continent?></h3>
            <ul class="list-unstyled text-center">
              <?php print _print_mirror_list($mir);?>
            </ul>
          <?php endforeach;?>
        </div>
      <?php endif;?>
    </div>
  </div>
</div>
<div id="rightcolumn" class="col-sm-6">
  <div class="padding-top-10 padding-bottom-30">
    <div class="eclipsefdn-promo-content text-center" data-ad-format="ads_square" data-ad-publish-to="eclipse_org_downloads"></div>
  </div>
  <div class="sideitem">
    <h6>Other options for this file</h6>
    <ul>
      <li><a href="download.php?file=<?php print $_file; ?>">All mirrors</a> (<a href="download.php?file=<?php print $_file; ?>&format=xml">xml</a>)</li>
      <li><a href="download.php?file=<?php print $_file; ?>&r=1">Direct link to file</a> (download starts immediately from best mirror)</li>
    </ul>
  </div>
  <div class="sideitem">
    <h6>Related Links</h6>
    <ul>
      <li><a href="/sponsor/">Sponsor</a></li>
      <li><a href="mir_request.php">Becoming a mirror site</a></li>
      <li><a href="https://help.eclipse.org/2020-06/index.jsp?topic=/org.eclipse.platform.doc.user/tasks/tasks-129.htm">Updating and installing Eclipse components</a></li>
      <li><a href="/forums/">Eclipse forums</a></li>
    </ul>
  </div>
</div>
