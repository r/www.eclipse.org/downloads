<?php
/*******************************************************************************
 * Copyright (c) 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Jens Reimann (IBH SYSTEMS GmbH) - initial implementation
 *
 *******************************************************************************/


while ($myrow = mysqli_fetch_assoc($rs)) {
	echo $myrow['base_path'] . $_file . "\n";
}
?>
