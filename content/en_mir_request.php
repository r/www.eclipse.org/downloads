<?php
/**
 * Copyright (c) 2013-2015, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation)- initial API and implementation
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

$level[0] = "";
$level[1] = "<img src=\"images/level1.jpg\" alt=\"\" />";
$level[2] = "<img src=\"images/level2.jpg\" alt=\"\" />";

if ($_status == "save") {
  $html = <<<EOHTML
<div id="maincontent">
  <div id="midcolumn">
    <h1>$pageTitle</h1>
    <h2>mirror site request result</h2>
EOHTML;

  switch ($error) {
    case 1:
      $html .= "<p>You have already requested a mirror, or the requested mirror already exists.
      <br /><br />Please contact the webmaster at webmaster@eclipse.org for assistance.</p>
      ";
      break;
    case 2:
      $html .= "<p>Some of the information you have provided does not make sense to us. If you're not some bot attempting to stuff bogus information in the form, please press back and revise your information.
        <p>The following field(s) are confusing us: " . $errorfield . "</p><p>
        Please contact the webmaster at webmaster@eclipse.org for assistance if needed.</p>";
      break;
    default:
      $html .= "<p>Thank you! <br /><br />Your mirror request has been submitted successfully.<br /><br />

      You may start RSYNCing from one of our rsync hubs.  Please see our <a href='eclipse.org-rsync.sh.txt'>RSYNC script</a> for information on the RSYNC hub closest to you.<br /><br />

      We will being checking your mirror periodically and once it seems healthy it will automatically added to the list of available mirrors.  If your mirror fails to appear within a few days of the RSYNC completing, please contact webmaster at eclipse dot org.<br /><br />

      Thanks again for volunteering to be an Eclipse mirror site!</p>
      ";
      break;
  }

  $html .= "
    </div>
  </div>
";

}
else {

  $html = <<<EOHTML
<div id="maincontent">
  <div id="midcolumn">
    <h1>$pageTitle</h1>
    <h2>Mirror site request form</h2>
    <p>Eclipse.org is looking for reliable sites with sufficient
bandwidth and stable internet connectivity to provide mirrors of the Eclipse
software downloads. You may also mirror the download site on your internal network.
    </p>
    <form name="form1" method="post" action="mir_request.php">
    <div class="homeitem3col">

      <h3>What are the requirements to become an Eclipse.org mirror?</h3>
      <ul>
          <li>An rsync client.</li>
        <li>1.5TB of disk space</li>
        <li>We ask that you run rsync at least once a day to update your mirror.
          Rsync updates will proceed more quickly between 4:00pm and 4:00am Eastern time, which represents the lowest bandwidth utilization period on Eclipse servers.</li>
        <li>Sufficient bandwidth to support the mirror.  We recommend at least 100 Mbps of available bandwidth.</li>
        <li>You must subscribe to the eclipse-mirrors@eclipse.org mailing list for mirror maintainers.  You can subscribe here: <a href="https://accounts.eclipse.org/mailing-list/eclipse-mirrors">https://accounts.eclipse.org/mailing-list/eclipse-mirrors</a></li>
        <li>Eclipse.org administrators may remove a mirror site from the list at any time, if we feel it is necessary.</li>
      </ul>
      <br /><br />
    </div>
    <div class="homeitem3col">
      <h3>How do I start ?</h3>
        <ul>
          <li>1. Fill out the following form:

            <table>
              <tr>
                <td>Organization/affiliation name (required)</td>
                <td>
                  <input type="text" name="organization" size="35" maxlength="50" /><br />
                </td>
              </tr>
                  <tr>
                          <td>Your name (required)</td>
                          <td><input type="text" name="contact" size="35" maxlength="50" /></td>
                  </tr>
                  <tr>
                          <td>Your e-mail address (required)</td>
                          <td><input type="text" name="email" size="35" maxlength="50" /></td>
                  </tr>
                  <tr>
                          <td>IP address (required)</td>
                          <td><input type="text" name="update_ip_allow" size="16" maxlength="15" /></td>
                  </tr>
                  <tr>
                          <td colspan="2"><font class="indexsub">Internet-facing IP address of the rsync client.  This can only be a single IP.</font></td>
                  </tr>
                  <tr>
                          <td>Country (required)</td>
                          <td>
                            <select name="ccode">
                              <option value="XX">Please select a country</option>
EOHTML;

  while ($myrow = mysqli_fetch_assoc($rs_country)) {
    $html .= "<option value='" . $myrow["ccode"] . "'>" . $myrow["en_description"] . "</option>";
  }
  $html .= <<<EOHTML
                            </select>
                          </td>
                  </tr>
              <tr>
                <td colspan="2">&#160;</td>
              </tr>
                  <tr>
                          <td>HTTPS URL (required)</td>
                          <td><input type="text" name="http_browserlink" size="35" maxlength="50" /></td>
                  </tr>
                  <tr>
                          <td colspan="2"><font class="indexsub">Base URL to the mirror. (https://www.yourdomain.com/eclipse)<br />
                          https required. Unencrypted http not supported.</font></td>
                  </tr>
                  <tr>
                          <td colspan="2">&#160;</td>
                  </tr>
                  <tr>
                          <td colspan="2">&#160;</td>
                  </tr>
                  <tr>
                          <td></td>
                  <td><input type="submit" value="Send my request" onclick="return fnValidForm();" /></td>
                  </tr>
            </table>
        </li>

        <li>2. Configure your RSYNC client and being RSYNC'ing.  You can download and use this shell script to mirror Eclipse: <a href="eclipse.org-rsync.sh.txt">eclipse.org-rsync.sh.txt</a>.
        <br />
        Contact webmaster@eclipse.org if you require further details.</li>
      </ul>
    </div>
      <input type="hidden" name="drop_id" value="" />
      <input type="hidden" name="status" value="save" />
    </form>
  </div>
  <div id="rightcolumn">
    <div class="sideitem">
      <h6>Related Links</h6>
      <ul>
        <li>Eclipse.org mirror RSYNC script: <a href="eclipse.org-rsync.sh.txt">eclipse.org-rsync.sh.txt</a></li>
        <li>Mirror site mailing list: <a href="https://dev.eclipse.org/mailman/listinfo/eclipse-mirrors">eclipse-mirrors</a></li>
      </ul>
    </div>
  </div>

</div>


<script language="javascript" type="text/javascript">
//<![CDATA[

  function fnValidForm() {

    isChecked = false;
    for(i = 0; i < document.form1.dropSelect.length; i++) {
      if(document.form1.dropSelect[i].checked) {
        isChecked = true;
        break;
      }
    }

    if(!isChecked) {
      alert("Please select the projects you wish to mirror.");
      return false;
    }

    if(document.form1.organization.value == "") {
      alert("Please enter your Organization.");
      document.form1.organization.focus();
      return false;
    }

    if(document.form1.contact.value == "") {
      alert("Please enter your Contact name.");
      document.form1.contact.focus();
      return false;
    }

    if(document.form1.email.value == "") {
      alert("Please enter your E-Mail address.");
      document.form1.email.focus();
      return false;
    }

    regExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/
    if(!regExp.test(document.form1.email.value)) {
      alert("Please enter a valid E-Mail address.");
      document.form1.email.focus();
      return false;
    }

    if(document.form1.update_ip_allow.value == "") {
      alert("Please enter your the IP address that must access the RSYNC server.");
      document.form1.update_ip_allow.focus();
      return false;
    }

    if(!isValidIPAddress(document.form1.update_ip_allow.value)) {
      alert("Please enter a valid IP address.");
      document.form1.update_ip_allow.focus();
      return false;
    }

    iptest = /^(192\.168|172\.16|10|224|240|127|0)\./;
    if(iptest.test(document.form1.update_ip_allow.value)) {
      alert("Private IP addresses are not permitted.");
      document.form1.update_ip_allow.focus();
      return false;
    }

    if(document.form1.ccode.selectedIndex == 0) {
      alert("Please select a country from the list.");
      return false;
    }

    if(document.form1.http_browserlink.value == "") {
      alert("Please enter the base URL for the http mirror.");
      document.form1.http_browserlink.focus();
      return false;
    }

    urltest = /^https:\/\/\w+\.[\w\/]+/;
    if(document.form1.http_browserlink.value != "" && !urltest.test(document.form1.http_browserlink.value)) {
      alert("Please enter a valid http URL.");
      document.form1.http_browserlink.focus();
      return false;
    }

    urltest = /^ftp:\/\/\w+\.[\w\/]+/;
    if(document.form1.ftp_browserlink.value != "" && !urltest.test(document.form1.ftp_browserlink.value)) {
      alert("Please enter a valid ftp URL.");
      document.form1.http_browserlink.focus();
      return false;
    }

    // Submit form.
    document.form1.drop_id.value = "";
    for(i = 0; i < document.form1.dropSelect.length; i++) {
      if(document.form1.dropSelect[i].checked) {
        if(document.form1.drop_id.value != "") {
          document.form1.drop_id.value += ",";
        }
        document.form1.drop_id.value += document.form1.dropSelect[i].value;
      }
    }
    return true;
  }

  function isValidIPAddress(ipaddr) {
     var re = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/;
     if (re.test(ipaddr)) {
        var parts = ipaddr.split(".");
        if (parseInt(parseFloat(parts[0])) == 0) { return false; }
        for (var i=0; i<parts.length; i++) {
           if (parseInt(parseFloat(parts[i])) > 255) { return false; }
        }
        return true;
     }
     else {
        return false;
     }
  }
//]]>
</script>

EOHTML;
}
