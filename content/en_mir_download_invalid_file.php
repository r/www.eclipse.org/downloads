<?php
	#*****************************************************************************
	#
	# en_mir_download_unavailable.php 
	#
	# Author: 		Denis Roy
	# Date:			2004-11-20
	#
	# Description: File Unavailable display
	#
	# HISTORY:
	#
	#****************************************************************************

	$pageTitle 		= "Eclipse downloads - file unavailable";


	#include("inc/en_banner.php");
	$html = <<<EOHTML
<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
		<p>
			<b><font size "+2">The selected file is invalid, or it is no longer available for download.</font></b>
        	<br /><br />
        	This happens when the file is part of a nightly or development build. If so, please return to the project's home page and download a newer version.
        	<br />
        	<br /><a href="javascript:history.go(-1)">Go back.</a>
		</p>
	</div>
</div>
EOHTML;
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

