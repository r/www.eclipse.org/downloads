<?php
/*******************************************************************************
 * Copyright (c) 2014,2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Christopher Guindon (Eclipse Foundation) - Initial implementation
 *******************************************************************************/

  require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
  require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
  require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");

  $App = new App();
  $Nav = new Nav();
  $Menu = new Menu();
  include_once($_SERVER['DOCUMENT_ROOT'] . "/downloads/_projectCommon.php");    # All on the same line to unclutter the user's desktop'

  # Begin: page-specific settings.  Change these.
  $pageTitle = "Thank You for Downloading Eclipse";
  // Bug 517126 - Thank you for downloading Eclipse
  $show_default_title = $App->getHTTPParameter('default_title');
  if ($show_default_title === "false") {
    $pageTitle .= ' technology';
  }

  $pageKeywords = "eclipse,download,downloads,mirror,project,plug-ins,plugins,java,ide,swt,refactoring,free java ide,tools,platform,open source,development environment,development,ide";
  $pageAuthor = "Christopher Guindon";

  //for testing, remove when done
  //$_url = 'http://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/juno/SR1/eclipse-jee-juno-SR1-win32.zip&url=http://download.eclipse.org/technology/epp/downloads/release/juno/SR1/eclipse-jee-juno-SR1-win32.zip&mirror_id=1';

  # Place your html content in a file called content/en_pagename.php
  $downloadBasepath = $App->getDownloadBasePath();
  // UnComment this line to restore the old download page.
  // include("content/en_" . $App->getScriptName());
  // Comment this line to restore the old download page.

  // Bug 484100 - Removed the Download button from mirror pages
  $variables = array();
  $variables['btn_cfa'] = array(
    'hide' => TRUE, // Optional - Hide the CFA button.
  );
  $App->setThemeVariables($variables);

  ob_start();
  include($_SERVER['DOCUMENT_ROOT'] . "/downloads/content/en_thankyou.php");
  $html = ob_get_clean();

  # Generate the web page
  if (isset($_url) && !$_debug) {
    $App->AddExtraHtmlHeader('<META HTTP-EQUIV="Refresh" CONTENT="0;URL=' . $_url . '">');
  }

  $App->generatePage(NULL, $Menu, NULL, $pageAuthor, $pageKeywords, $pageTitle, $html);
