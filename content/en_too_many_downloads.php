<?php
	#*****************************************************************************
	#
	# en_too_many_downloads.php 
	#
	# Author: 		Denis Roy
	# Date:			2012-04-05
	#
	# Description: Error message for too many downloads
	#
	# HISTORY:
	#
	#****************************************************************************

	$pageTitle 		= "Eclipse downloads - too frequent";


	#include("inc/en_banner.php");
	$html = <<<EOHTML
<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
		<p>
			<b><font size "+2">You have exceeded the amount of times you can call this page.</font></b>
        	<br /><br />
        	Please wait a while and try later.  If you believe this is happening erroneously, please contact webmaster@eclipse.org.
        	<br />
        	<br /><a href="javascript:history.go(-1)">Go back.</a>
		</p>
	</div>
</div>
EOHTML;
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

